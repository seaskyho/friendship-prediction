# Pairwise Relationship

This project use Neural Network with TensorFlow as the training model to predict if an edge will form between two nodes in the future.

### Report
For a detailed introduction and analysis of the project, please refer to the file **report.pdf**.

### Pre-requisite

Before running, make sure you have the environment set up as followed:

* python3
* tensorflow

### Running

To run the program, run
```
sh run.sh
```
### File Structure

```bash
├── assets
│   ├── test-public.txt
│   └── train.txt
├── src
│   ├── generate_all_node.py
│   ├── generate_dictionary.py
│   ├── generate_trainingdata.py
│   ├── util.py
│   └── main.py
├── run.sh
└── README.md
```

**test-public.txt** is the input file for test purpose.

**train.txt** is the input file of all training data.

**generate_all_node.py** is the file to generate the sink_source_count.csv, which indicates the number of source and sink of each node.

**generate_all_dictionary.py** is the file that generate all the database files used to facilitate the process of feature calculation and model training.

**generate_trainingdata.py** is the file that generate the training data.

**util.py** includes all the sqlite util functions.

**main.py** is the entrance of the whole program. 

### Facts to note
1. Before running, test-public.txt and train.txt should be put inside **/assets** and named exactly as what they are
2. While running, the program will create two folders, one is **/database** and another one is **/output**.
The former one is where all the .db files are generated. 
The later one is where the final result is generated. Take note not to delete or modify the files in /database while the program is still running.
3. After running, the program will generate files with size around 1.2GB, please make sure enough space is available for the running environment.
