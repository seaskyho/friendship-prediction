from collections import defaultdict
import csv
import os

file = open('../assets/train.txt', 'r')
edge_list = defaultdict(list)
for line in file:
    line_list = line.strip('\n')
    line_list = line_list.split('\t')
    edge_list[line_list[0]] = line_list[1:]
edge_count_out = defaultdict(int)
for key in edge_list.keys():
    edge_count_out[key] = len(edge_list[key])
edge_list_in = defaultdict(list)
for key in edge_list.keys():
    for node in edge_list.get(key):
        if not edge_list_in[node]:
            edge_list_in[node] = [key]
        else:
            edge_list_in[node].append(key)
edge_count_in = defaultdict(int)
for key in edge_list_in.keys():
    edge_count_in[key] = len(edge_list_in[key])

filename = "../database/"
os.makedirs(os.path.dirname(filename), exist_ok=True)
f = open("../database/sink_source_count.csv", "w")
w = csv.writer(f)
for key in edge_list.keys():
    w.writerow([key, edge_count_out[key], edge_count_in[key]])
f.close()


def allSet():
    source_dict = set()
    with open("../database/sink_source_count.csv") as training_data:
        raw_data = training_data.readlines()
        for raw_line in raw_data:
            line = raw_line.strip().split(',')
            source_dict.add(line[0])
    training_data.close()
    return source_dict
