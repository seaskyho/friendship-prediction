# this file contains all utility functions that can be shared by other files
import sqlite3


# create a sqlite database handler
def createDBHandler(path):
    db = sqlite3.connect(path)
    c = db.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS map (id INTEGER UNIQUE, k TEXT UNIQUE, v TEXT)''')
    db.commit()
    return db


# create a sqlite training database handler
def createTrainingDBHandler(path):
    db = sqlite3.connect(path)
    c = db.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS map (k TEXT, v1 TEXT, v2 INTEGER)''')
    db.commit()
    return db


# save a row of training dictionary into database
def save2TrainingDB(db, key, v1, v2):
    cursor = db.cursor()
    cursor.execute("""INSERT INTO map VALUES (?,?,?)""", (key, v1, v2))
    db.commit()


# save a row of dictionary into database
def save2DB(id, key, value, db):
    cursor = db.cursor()
    cursor.execute("""INSERT INTO map VALUES (?,?,?)""", (id, key, value))
    db.commit()


# fetch value from database given a key
def getFromDB(key, db, default=None):
    cursor = db.cursor()
    result = cursor.execute("""SELECT v FROM map WHERE k = ?""", (key,)).fetchone()
    if result is None:
        return default
    return result[0]


# fetch value from database given an ID
def getById(db, id, default=None):
    cursor = db.cursor()
    result = cursor.execute("""SELECT * FROM map WHERE id = ?""", (id,)).fetchone()
    if result is None:
        return default
    return result
