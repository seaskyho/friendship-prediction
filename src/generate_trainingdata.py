from util import *
import random
from generate_all_node import *
from random import randint

# connect to sink_dict.db
training_db = createTrainingDBHandler("../database/training_dict.db")
sink_db = createDBHandler("../database/sink_dict.db")
s = allSet()
l = list(s)
length = len(l)

times = 40000
while times > 0:
    # randomly choose an key pair from sink_db
    rowID1 = randint(0, 19999)
    (_, key1, value1_str) = getById(sink_db, rowID1)
    value1 = eval(value1_str)
    if len(value1) == 0:
        continue
    # randomly choose an value from a list of values
    v1 = random.sample(value1, 1)[0]
    save2TrainingDB(training_db, key1, v1, 1)
    # ramdomly choose a key that not in value1
    row2ID = randint(0, length - 1)
    while l[row2ID] in value1:
        row2ID = randint(0, length - 1)
    save2TrainingDB(training_db, key1, l[row2ID], 0)
    times -= 1
