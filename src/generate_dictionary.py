from collections import defaultdict
import os
from util import save2DB, createDBHandler

filename = "../database/"
os.makedirs(os.path.dirname(filename), exist_ok=True)

# connect to sink_dict.db
sink_db = createDBHandler("../database/sink_dict.db")

# connect to source_dict.db
source_db = createDBHandler("../database/source_dict.db")

"""
* FUNCTIONALITY: build sink/source dictionary
* KEY: (id) string
* VALUE: set of (id) strings
"""

source_dict = defaultdict(set)

with open("../assets/train.txt") as training_data:
    raw_data = training_data.readlines()
    i = 0
    for raw_line in raw_data:
        line = raw_line.strip().split()
        save2DB(i, line[0], repr(set(line[1:])), sink_db)  # build sink_dict db
        i += 1
        for item in line[1:]:
            source_dict[item].add(line[0])
training_data.close()

# build source_dict db
i = 0
for key, value in source_dict.items():
    save2DB(i, key, repr(value), source_db)
    i += 1

"""END"""
