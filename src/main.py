from __future__ import absolute_import, division, print_function

import csv
import datetime
import time
import os

import numpy as np
import tensorflow as tf
from tensorflow import keras

from util import getFromDB, createDBHandler

start = time.time()

# connect to sink_dict.db
sink_db = createDBHandler("../database/sink_dict.db")

# connect to source_dict.db
source_db = createDBHandler("../database/source_dict.db")


# fetch sink dictionary value according to id
def fetch_sink_dict(id):
    rawdict = getFromDB(id, sink_db)
    if rawdict is None:
        return set()
    else:
        return eval(rawdict)


# fetch source dictionary value according to id
def fetch_source_dict(id):
    rawdict = getFromDB(id, source_db)
    if rawdict is None:
        return set()
    else:
        return eval(rawdict)


""" **** build up features (assume A -> B) *** """


def build_features(userA, userB):
    """
    Find the features (assume A->B)

    :param userA: (string) user A's id
    :param userB: (string) user B's id
    :return: (list) a list of features
    """

    # the following four variables has type "set"
    a_sink = fetch_sink_dict(userA)
    a_source = fetch_source_dict(userA)
    b_sink = fetch_sink_dict(userB)
    b_source = fetch_source_dict(userB)

    a_sink_len = len(a_sink)  # feature 1: how many ppl does A follow
    a_source_len = len(a_source)
    b_sink_len = len(b_sink)
    b_source_len = len(b_source)  # feature 2: how many ppl does B get followed by

    common_friends = a_sink & a_source & b_sink & b_source
    common_friends_len = len(common_friends)  # feature 3: how many common friends do A and B share

    '''Option 2: Dice Similarity'''
    if a_sink_len == 0 and b_sink_len == 0:
        sink_simi = 0  # feature 4: dice similarity between A and B's sink set
    else:
        sink_simi = 2 * len(a_sink & b_sink) / (a_sink_len + b_sink_len)

    if a_source_len == 0 and b_source_len == 0:
        source_simi = 0  # feature 5: dice similarity between A and B's source set
    else:
        source_simi = 2 * len(a_source & b_source) / (a_source_len + b_source_len)

    if a_sink_len == 0 and b_source_len == 0:
        neighbour_simi = 0  # feature 6: dice similarity between A and B's neighbour
    else:
        neighbour_simi = 2 * len(a_sink & b_source) / (a_sink_len + b_source_len)
    return [a_sink_len, b_source_len, common_friends_len, sink_simi, source_simi, neighbour_simi]


""" **** train the model *** """

x_values = []
y_values = []

line = 0

train_db = createDBHandler("../database/training_dict.db")
cursor = train_db.cursor()
cursor.execute('''SELECT * FROM map''')
for (a, b, label) in cursor:
    line += 1
    features = build_features(a, b)
    x_values.append(features)
    y_values.append(label)
    if line % 1000 == 0:
        print("at line", line)

x = np.array(x_values)
y = np.array(y_values)

# normalize
mean = x.mean(axis=0)
std = x.std(axis=0)
x = (x - mean) / std

print('finish X, Y build-up')


def build_model():
    model = keras.Sequential([
        keras.layers.Dense(64, activation=tf.nn.relu, kernel_regularizer=keras.regularizers.l2(0.001),
                           input_shape=(x.shape[1],)),
        keras.layers.Dense(64, activation=tf.nn.relu, kernel_regularizer=keras.regularizers.l2(0.001)),
        keras.layers.Dense(1)
    ])

    optimizer = tf.train.RMSPropOptimizer(0.001)

    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae'])
    return model


model = build_model()

EPOCHS = 500

# The patience parameter is the amount of epochs to check for improvement
early_stop = keras.callbacks.EarlyStopping(monitor='val_loss', patience=20)

# Store training stats
history = model.fit(x, y, epochs=EPOCHS, validation_split=0.2, verbose=0, callbacks=[early_stop])

print('finish training')

""" **** let's predict *** """

test_features = []
with open('../assets/test-public.txt', 'r') as test_doc:
    raw_data = test_doc.readlines()
    for raw_line in raw_data[1:]:
        line = raw_line.strip().split()
        id, a, b = line[0], line[1], line[2]
        features = build_features(a, b)
        test_features.append(features)
test_doc.close()

test_features = (np.array(test_features) - mean) / std

predictions = model.predict(test_features).flatten()

print("finish prediction")

""" **** write to .csv *** """
timestamp = str(datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d-%H%M%S'))
filename = "../output/"
os.makedirs(os.path.dirname(filename), exist_ok=True)
with open('../output/prediction-' + timestamp + '.csv', 'w') as output_file:
    writer = csv.writer(output_file, delimiter=',')
    writer.writerow(["Id", "Prediction"])
    for i in range(len(predictions)):
        pred_label = predictions[i]
        if pred_label > 1:
            pred_label = 1
        elif pred_label < 0:
            pred_label = 0
        writer.writerow([i + 1, pred_label])
output_file.close()

print("finish writing to output")

end = time.time()
print("used time:", end - start)
